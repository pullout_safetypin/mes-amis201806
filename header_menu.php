    <div>
      <h1 class="mainlogo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/common/mes-amis-logo-bk.svg" alt="メザミバレエスタジオ"></a></h1>
      <nav>
        <section class="menu">
          <section class="btns">
            <a class="access" href="/access/"><div><span class="en">ACCESS</span><span class="jp">アクセス</span></div></a>
            <a class="schedule" href="/schedule/"><div><span class="en">SCHEDULE</span><span class="jp">スケジュール</span></div></a>
            <a class="contact" href="/contact/"><div><span class="en">CONTACT</span><span class="jp">体験／申込</span></div></a>
          </section>
          <div class="link">
            <section>
              <a href="/about/" class="about"><span class="en">ABOUT</span><span class="jp">メザミについて</span></a>
              <a href="/teacher/" class="teacher"><span class="en">TEACHER</span><span class="jp">講師の紹介</span></a>
              <a href="/classes/" class="classes"><span class="en">CLASSES</span><span class="jp">クラスの紹介</span></a>
              <?php
              $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'category_name' => 'event'
              );
              $the_query = new WP_Query($args);
              if ( $the_query->have_posts() ) :
                echo('<a href="/event/" class="event"><span class="en">EVENT</span><span class="jp">イベント</span></a>');
              ?>
              <?php endif; ?>
              <a href="/gellary/" class="gellary"><span class="en">GALLERY</span><span class="jp">ギャラリー</span></a>
              <a href="/qa/" class="qa"><span class="en">Q&amp;A</span><span class="jp">よくある質問</span></a>
            </section>
            <section class="sns">
              <a class="insta" href="https://www.instagram.com/mesamisballetstudio/" target="_blank" title="Instagram"></a>
              <a class="fb" href="https://www.facebook.com/mesamisballet/" target="_blank" title="Facebook"></a>
            </section>
          </div>
        </section>
        <section class="sp_menu">
          <section class="btns">
            <a class="sp_menu_btn"></a>
          </section>
        </section>
      </nav>
    </div>