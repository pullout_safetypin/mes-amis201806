<?php /* Template Name: news */ ?>
<?php get_header(); ?>
<?php 
$cat_info = get_category($cat); 
?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/home.css?20241128" type="text/css">
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section id="contents">
    <section>
      <h1 class="content-title"><span>全ての<?php echo wp_specialchars( $cat_info->name );  ?></span></h1>
      <ul class="post_list">
      <?php
      $paged = (int) get_query_var('paged');
      $args = array(
        'paged' => $paged,
        'posts_per_page' => 15,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'post',
        'post_status' => 'publish',
        'category_name' => $cat_info->slug
      );

      $the_query = new WP_Query($args);
      if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
      <li <?php if( (date('Ymd') - get_post_time('Ymd')) < 14 ){echo('class="new"');} ?>>
        <a href="<?php the_permalink(); ?>">
          <div class="img">
            <?php
            if( has_post_thumbnail() ){
              the_post_thumbnail('medium');
            }
            else{
              echo ('<img src="'.get_template_directory_uri().'/assets/img/common/icon_amisanyosan.jpg">');
            }
            ?>
          </div>
          <div class="info">
            <p class="release_date"><?php echo get_post_time('Y年n月j日'); ?></p>
            <p class="title"><?php the_title(); ?></p>
          </div>
        </a>
      </li>
      <?php endwhile; endif; ?>
      </ul>
      <section class="pager">
        <?php
        if ($the_query->max_num_pages > 1) {
          echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => 'page/%#%/',
            'current' => max(1, $paged),
            'total' => $the_query->max_num_pages,
            'prev_text' => __('< PREV'),
            'next_text' => __('NEXT >'),
          ));
        }
        ?>
        <?php wp_reset_postdata(); ?>
      </section>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>