    <nav>
      <div>
        <h1><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/common/mes-amis-logo-w.svg"></a></h1>
        <span class="btns"><a class="switch_viewport_pc">パソコン用表示に切替</a></span>
      </div>
      <div class="link">
        <section class="sp_close">
          <span></span>
        </section>
        <section>
          <a href="/about/" class="about"><span class="en">ABOUT</span><span class="jp">メザミについて</span></a>
          <a href="/teacher/" class="teacher"><span class="en">TEACHER</span><span class="jp">講師の紹介</span></a>
          <a href="/classes/" class="classes"><span class="en">CLASSES</span><span class="jp">クラスの紹介</span></a>
          <?php
          $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'category_name' => 'event'
          );
          $the_query = new WP_Query($args);
          if ( $the_query->have_posts() ) :
            echo('<a href="/event/" class="event"><span class="en">EVENT</span><span class="jp">イベント</span></a>');
          ?>
          <?php endif; ?>
          <a href="/gellary/" class="gellary"><span class="en">GALLERY</span><span class="jp">ギャラリー</span></a>
          <a href="/qa/" class="qa"><span class="en">Q&amp;A</span><span class="jp">よくある質問</span></a>
          <a href="/access/"><span class="en">ACCESS</span><span class="jp">スタジオへのアクセス</span></a>
          <a href="/schedule/"><span class="en">SCHEDULE</span><span class="jp">スケジュール</span></a>
          <a href="/contact/"><span class="en">CONTACT</span><span class="jp">体験／申込</span></a>
        </section>
        <section class="sns">
          <a class="insta" href="https://www.instagram.com/mesamisballetstudio/" target="_blank" title="Instagram"></a>
          <a class="fb" href="https://www.facebook.com/mesamisballet/" target="_blank" title="Facebook"></a>
        </section>
      </div>
    </nav>    
