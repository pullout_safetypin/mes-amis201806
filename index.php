<?php /* Template Name: home */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/top.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/vegas/vegas.css">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/vegas/vegas.min.js"></script>
  <script>
    $(document).ready(function(){
      //set images to slide
      $('#slider').vegas({
        delay: 6000,
        timer: false,
        slides:[
          { src: "<?php echo get_template_directory_uri(); ?>/assets/img/slide/slide_01.jpg" },
          { src: "<?php echo get_template_directory_uri(); ?>/assets/img/slide/slide_02.jpg" },
          { src: "<?php echo get_template_directory_uri(); ?>/assets/img/slide/slide_03.jpg" },
          { src: "<?php echo get_template_directory_uri(); ?>/assets/img/slide/slide_05.jpg" },
          { src: "<?php echo get_template_directory_uri(); ?>/assets/img/slide/slide_04.jpg" },
        ]
      });
    });
  </script>
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="slider"></section>
  <section id="contents">
    <section id="about">
      <div class="wrapper">
        <section class="frame">
          <div>
            <div>
              <h2 class="content-title"><span>お知らせ</span></h2>
              <ul id="news_body">
                <?php
                $args = array(
                  'posts_per_page' => 4,
                  'orderby' => 'post_date',
                  'order' => 'DESC',
                  'post_type' => 'post',
                  'post_status' => 'publish',
                  'category_name' => 'news'
                );
                $the_query = new WP_Query($args);
                if ( $the_query->have_posts() ) :
                  while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>
                <li <?php if( (date('Ymd') - get_post_time('Ymd')) < 14 ){echo('class="new"');} ?>>
                  <a href="<?php the_permalink(); ?>">
                    <div>
                      <p class="release_date"><?php echo get_post_time('Y年n月j日'); ?></p>
                      <p class="title">
                        <?php 
                        if(mb_strlen($post->post_title)>23) {
                          $title= mb_substr($post->post_title,0,23) ;
                          echo $title.'...';
                        }
                        else {
                          echo $post->post_title;
                        }
                        ?>
                      </p>
                    </div>
                    <?php
                    if( has_post_thumbnail() ){

                      echo('<div style="background-image:url('."'".get_the_post_thumbnail_url( get_the_ID(), 'medium' )."'".');"></div>');
//                      echo ('<img src="'.get_template_directory_uri().'/assets/img/common/icon_amisanyosan.jpg">');
//                      the_post_thumbnail('large');
                    }
                    else{
                      echo('<div style="background-image:url('."'".get_template_directory_uri().'/assets/img/common/icon_amisanyosan.jpg'."'".');"></div>');
//                      echo ('<img src="'..'">');
                    }
                    ?>
                  </a>
                </li>
                <?php endwhile; endif; ?>
              </ul>
              <a class="read_more" href="/blog/">SHOW ALL NEWS</a>
            </div>
          </div>
        </section>
        <section class="frame">
          <div>
            <div>
              <h2 class="content-title"><span>メザミについて</span></h2>
              <p>MES AMIS BALLET STUDIOは門前仲町、清澄白河のバレエスタジオです。<br>膝に負担がかからないよう、独特の工法で施工されたバレエ専門床。約100㎡の広々とした明るい環境です。</p>
              <div class='studioimage'></div>
              <span class="read_more_bg"><a class="read_more" href="/about/">READ MORE</a></span>
            </div>
          </div>
        </section>
      </div>
    </section>
    <section id="teacher">
      <div class="wrapper">
        <div><!-- For teacher image --></div>
        <div>
          <div><!-- For bold frame --></div>
          <div>
            <h2 class="content-title"><span>講師の紹介</span></h2>
            <p>海外でプロとして活躍し、豊かな経験をもつ講師。</p>
            <p>優れた技術指導はもちろん、高い芸術性を育むために、きめ細やかな指導、情操教育を行い人格的にも世界に通用するダンサーへと育てます。</p>
            <div class="teacherimage"></div>
            <a class="read_more" href="/teacher/">READ MORE</a>
          </div>
        </div>
      </div>
    </section>
    <section id="access">
      <div class="wrapper">
        <div>
          <div><!-- For bold frame --></div>
          <div>
            <h2 class="content-title"><span>アクセス</span></h2>
            <p>メザミバレエスタジオは門前仲町駅から徒歩7分、清澄白河駅から徒歩10分。江東区内はもちろん、港区、中央区、千代田区、墨田区、江戸川区など、近隣の区から通う生徒さんもいらっしゃいます。<br>
            お気軽にお問合わせください。</p>
            <div class="accessimage"></div>
            <a class="read_more" href="/access/">READ MORE</a>
          </div>
        </div>
        <div><!-- For access image --></div>
      </div>
    </section>
    <section id="others">
      <div class="wrapper">
        <section class="frame">
          <div>
            <div>
              <h2 class="content-title"><span>スケジュール</span></h2>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/schedule.jpg" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/top/schedule.jpg 1x, <?php echo get_template_directory_uri(); ?>/assets/img/top/schedule_retina.jpg 2x" alt="">
              <span class="read_more_bg"><a class="read_more" href="/schedule/">READ MORE</a></span>
            </div>
          </div>
        </section>
        <section class="frame">
          <div>
            <div>
              <h2 class="content-title"><span>イベント</span></h2>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/event.jpg" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/top/event.jpg 1x, <?php echo get_template_directory_uri(); ?>/assets/img/top/event_retina.jpg 2x" alt="">
              <span class="read_more_bg"><a class="read_more" href="/event/">READ MORE</a></span>
            </div>
          </div>
        </section>
        <section class="frame">
          <div>
            <div>
              <h2 class="content-title"><span>ギャラリー</span></h2>
              <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/gellary.jpg" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/top/gellary.jpg 1x, <?php echo get_template_directory_uri(); ?>/assets/img/top/gellary_retina.jpg 2x" alt="">
              <span class="read_more_bg"><a class="read_more" href="/gellary/">READ MORE</a></span>
            </div>
          </div>
        </section>
      </div>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>