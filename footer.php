    <div>
      <div class="footer_left">
        <div>
          <div>
            <h1 class="mainlogo"><a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/common/mes-amis-logo-bk.svg"></a></h1>
            <p>〒135-0033</p>
            <p>東京都江東区深川<br class="only-sp">2-26-8-1F</p>
            <p>TEL:090-2440-2992</p>
          </div>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/common/amisanyosan.jpg" srcset="<?php echo get_template_directory_uri(); ?>/assets/img/common/amisanyosan.jpg 1x, <?php echo get_template_directory_uri(); ?>/assets/img/common/amisanyosan_retina.jpg 2x" alt="">
        </div>
        <p class="copyright">Copyright <span>&copy;</span> MES AMIS BALLET STUDIO<br class="only-sp"> All Rights Reserved.</p>        
      </div>
      <div class="footer_right">
        <section class="btns">
          <a class="contact" href="/contact/"><span>見学／体験レッスンのお問い合わせ</span></a>
        </section>
        <div>
          <section class="sns">
            <section class="btns">
              <a class="access" href="/access/"><span>スタジオへのアクセス</span></a>
            </section>
            <a class="insta" href="https://www.instagram.com/mesamisballetstudio/" target="_blank"></a>
            <a class="fb" href="https://www.facebook.com/mesamisballet/" target="_blank"></a>
          </section>
          <nav>
            <section class="link">
              <div>
                <a href="/about/">メザミについて</a>
                <a href="/teacher/">講師の紹介</a>
                <a href="/classes/">クラスの紹介</a>
              </div>
              <div>
                <?php
                $args = array(
                  'post_type' => 'post',
                  'post_status' => 'publish',
                  'category_name' => 'event'
                );
                $the_query = new WP_Query($args);
                if ( $the_query->have_posts() ){
                  echo('<a href="/event/">イベント</a>');
                }
                else{
                  echo('<a style="height:0px; overflow:hidden; padding:0;" href="/event/">イベント</a>');
                }
                ?>
                <a href="/gellary/">ギャラリー</a>
                <a href="/qa/">よくある質問</a>
              </div>
            </section>
          </nav>
        </div>
        <p class="copyright">Copyright <span>&copy;</span> MES AMIS BALLET STUDIO All Rights Reserved.</p>
      </div>

    </div>
