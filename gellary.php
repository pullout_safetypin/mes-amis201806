<?php /* Template Name: gellary */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/baguetteBox/baguetteBox.min.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/gellary.css?20181204" type="text/css">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/baguetteBox/baguetteBox.min.js"></script>
  <script>
    $(document).ready(function(){
      var first_recital = baguetteBox.run('.first_recital',{
        buttons:true,
      });
      $('.first_recital').on('click',function(){
        baguetteBox.show(1, first_recital[0]);
      });
    });
  </script>
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section>
      <h1 class="content-title"><span>ギャラリー</span></h1>
      <section class="gellary">
        <section class="first_recital">
          <a class="show_gellary" href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/40006.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/40006.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/40006.jpg"
            style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/1st_recital_thum_nail.jpg);">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0083.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0083.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/0083.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0795.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0795.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/0795.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0922.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/0922.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/0922.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1117.jpg"
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1117.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/1117.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1158.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1158.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/1158.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1165.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1165.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/1165.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1390.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1390.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/1390.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1822.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/1822.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/1822.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2488.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2488.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/2488.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2506.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2506.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/2506.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2736.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2736.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/2736.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2952.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2952.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/2952.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2997.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/2997.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/2997.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/3003.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/3003.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/3003.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/40092.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/40092.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/40092.jpg">
          </a>
          <a href="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/60001.jpg" 
            data-at-768="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/full/60001.jpg" 
            data-at-414="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/2018_first_recital/medium/60001.jpg">
          </a>          
        </section>
        <section class="fb_link">
          <a href="https://www.facebook.com/pg/mesamisballet/photos/" target="_blank" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/gellary/facebook_link.jpg);"></a>
        </section>
      </section>
      <!--section class="fb_link">
        <a class="" href="https://www.facebook.com/pg/mesamisballet/photos/" target="_blank">
          <span>Facebookでは普段のスタジオの様子を更新中！</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/gellary/gellary.jpg">
        </a>
      </section-->
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>