<?php /* Template Name: access */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="top_part">
    <div class="wrapper">
      <section>
        <h1 class="main-title"><span>スタジオへのアクセス</span></h1>
        <p>
          メザミバレエスタジオは大江戸線、東西線の門前仲町駅から徒歩7分、大江戸線、半蔵門線の清澄白河駅から徒歩10分。<br>
          江東区だけでなく中央区、千代田区、墨田区、江戸川区など区外から通う生徒さんもいらっしゃいます。
        </p>
      </section>
      <section>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/access_top.jpg">
      </section>
    </div>
  </section>
  <section id="contents">
    <section class="sec-access">
      <div>
        <div>
          <h2>MES AMIS BALLET STUDIO <span>−メザミバレエスタジオ−</span></h2>
          <p>〒135-0033 東京都江東区深川2-26-8-1F<br>TEL：090-2440-2992</p>
          <ul class="dot">
            <li>門前仲町駅から：<br>徒歩7分</li>
            <li>清澄白河駅から：<br>徒歩10分</li>
            <li>都営バス 有明/豊洲方面から：<br>海01/門19系統 「門前仲町」下車 徒歩6分</li>
            <li>勝どき/月島/清澄白河/両国/押上/亀戸方面から：<br>門33系統「深川二丁目」下車 徒歩3分</li>
          </ul>
          <!--div class="btns"><a href="howtowalk.php">門前仲町駅からの歩き方</a></div-->
        </div>
        <div>
          <iframe class="gmap" style="border: 0;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6482.082256507514!2d139.79503443292663!3d35.67598928019551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601889114ed7c6d5%3A0x6b417fe182e1f5b3!2z44CSMTM1LTAwMzMg5p2x5Lqs6YO95rGf5p2x5Yy65rex5bed77yS5LiB55uu77yS77yW4oiS77yY!5e0!3m2!1sja!2sjp!4v1467379666817" width="576" height="100%" frameborder="0"></iframe>
        </div>
      </div>
    </section>

    <section class="sec-route">
      <h2 class="content-title"><span>各区からのアクセス</span></h2>
      <div class="sec-route__wrap">
        <picture class="sec-route__image">
            <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/top/access_image_sp.png" media="(max-width:768px)">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/access_image.png" alt="">
        </picture>
        <div class="sec-route__links">
          <a class="item" href="/access/from-edogawa-ku/">江戸川区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-sumida-ku/">墨田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chuo-ku/">中央区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chiyoda-ku/">千代田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-minato-ku/">港区からの<br class="only-sp">アクセス</a>
        </div>              
      </div>

    </section>

  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>