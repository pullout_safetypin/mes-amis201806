<?php /* Template Name: access-from-sumida-ku */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access-sub.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section id="sumida">
      <div class="nav-wrap">
        <h1 class="title only-sp"><span>墨田区からのアクセス </span></h1>
        <picture class="image">
          <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-sumida-sp.png" media="(max-width:768px)">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-sumida.png" alt="">
        </picture>
        <div>
          <nav class="sec-links">
            <h1 class="title only-pc"><span>墨田区からのアクセス </span></h1>
            <div class="by-train">電車でお越しの方</div>
            <ul class="link-list">
              <li class="item"><a href="#sumida_ryougoku01">両国駅から</a></li>
              <li class="item"><a href="#sumida_kinshichou01">錦糸町駅から</a></li>
              <li class="item"><a href="#sumida_oshiage01">押上駅から</a></li>
            </ul>
            <div class="by-bus">バスでお越しの方</div>
            <ul class="link-list">
              <li class="item"><a href="#sumida_ryougoku02">両国駅から</a></li>
              <li class="item"><a href="#sumida_kikukawa02">菊川駅から</a></li>
              <li class="item"><a href="#sumida_oshiage02">押上駅から</a></li>
              <li class="item"><a href="#sumida_skytree02">とうきょうスカイツリー駅から</a></li>
              <li class="item"><a href="#sumida_kinshichou02">錦糸町駅から</a></li>
            </ul>
            <div class="by-car">車でお越しの方</div>
            <ul class="link-list">
              <li class="item"><a href="#sumida_car">両国駅から</a></li>
              <li class="item"><a href="#sumida_car">錦糸町駅から</a></li>
              <li class="item"><a href="#sumida_car">押上駅から</a></li>
            </ul>
          </nav>
        </div>
      </div>

      <h2 class="title by-train">電車でお越しの方</h2>
      <section id="sumida_ryougoku01" class="data-wrap">
        <h3 class="sub-title">両国駅からお越しの方<br class="only-sp">（乗り入れ線：都営大江戸線、JR総武線）</h3>
        <p class="summary">都営大江戸線「両国駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>3分</p>
          <p><span>所要時間</span>13分</p>
        </div>
        <div class="route">
          <p><span class="ooedo">都営大江戸線</span>「両国駅」乗車</p>
          <p>↓大門・六本木方面（2駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「清澄白河駅」下車</p>
        </div>
      </section>
      <section id="sumida_kinshichou01" class="data-wrap">
        <h3 class="sub-title">錦糸町駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ半蔵門線、JR総武線）</h3>
        <p class="summary">東京メトロ半蔵門線「錦糸町駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>4分</p>
          <p><span>所要時間</span>14分</p>
        </div>
        <div class="route">
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「錦糸町駅」乗車</p>
          <p>↓中央林間行き（2駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>
      <section id="sumida_oshiage01" class="data-wrap">
        <h3 class="sub-title">押上駅からお越しの方<br class="only-sp">（乗り入れ線：京成押上線、東京メトロ半蔵門線、東武伊勢崎線、都営浅草線）</h3>
        <p class="summary">東京メトロ半蔵門線「押上駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>6分</p>
          <p><span>所要時間</span>16分</p>
        </div>
        <div class="route">
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「押上駅」乗車</p>
          <p>↓中央林間行き（3駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>

      <h2 class="title by-bus">バスでお越しの方</h2>
      <section id="sumida_ryougoku02"  class="data-wrap">
        <h3 class="sub-title">両国駅からお越しの方</h3>
        <p class="summary">「両国駅」A4出口徒歩1分、都営バス門33 豊海水産埠頭行き、「都営両国駅前」乗車、「深川二丁目」下車、メザミバレエスタジオ徒歩3分</p>
        <div class="data">
          <p><span>乗車時間</span>11分</p>
          <p><span>所要時間</span>15分</p>
        </div>
        <div class="route">
          <p>「両国駅」A4出口</p>
          <p>↓徒歩1分</p>
          <p><span class="toei-bus">都営バス</span> 門33 「都営両国駅前」乗車</p>
          <p>↓豊海水産埠頭行き　（8駅）</p>
          <p><span class="toei-bus">都営バス</span> 門33 「深川二丁目」下車</p>
        </div>
      </section>
      <section id="sumida_kikukawa02"  class="data-wrap">
        <h3 class="sub-title">菊川駅からお越しの方</h3>
        <p class="summary">「菊川駅」A1番出口より徒歩1分、都営バス東20 東京駅丸の内北口行き「菊川駅前」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
        <div class="data">
          <p><span>乗車時間</span>17分</p>
          <p><span>所要時間</span>24分</p>
        </div>
        <div class="route">
          <p>「菊川駅」A1番出口</p>
          <p>↓徒歩1分</p>
          <p><span class="toei-bus">都営バス</span> 東20 「菊川駅前」乗車</p>
          <p>↓東京駅丸の内北行き （12駅）</p>
          <p><span class="toei-bus">都営バス</span> 東20 「門前仲町」下車</p>
        </div>
      </section>
      <section id="sumida_oshiage02"  class="data-wrap">
        <h3 class="sub-title">押上駅からお越しの方</h3>
        <p class="summary">「押上駅（スカイツリー前）」A2番出口より徒歩1分、都営バス門33 豊海水産埠頭行き「押上」乗車、「深川二丁目」下車、メザミバレエスタジオ徒歩3分</p>
        <div class="data">
          <p><span>乗車時間</span>22分</p>
          <p><span>所要時間</span>26分</p>
        </div>
        <div class="route">
          <p>「押上駅（スカイツリー前）」A2番出口</p>
          <p>↓徒歩1分</p>
          <p><span class="toei-bus">都営バス</span> 門33 「押上」乗車</p>
          <p>↓豊海水産埠頭行き　（15駅）</p>
          <p><span class="toei-bus">都営バス</span> 門33 「深川二丁目」下車</p>
        </div>
      </section>
      <section id="sumida_skytree02"  class="data-wrap">
        <h3 class="sub-title">とうきょうスカイツリー駅からお越しの方</h3>
        <p class="summary">「とうきょうスカイツリー駅」東改札口より徒歩2分、都営バス門33 豊海水産埠頭行き「とうきょうスカイツリー駅入口」乗車、「深川二丁目」下車、メザミバレエスタジオ徒歩3分</p>
        <div class="data">
          <p><span>乗車時間</span>22分</p>
          <p><span>所要時間</span>27分</p>
        </div>
        <div class="route">
          <p>「とうきょうスカイツリー駅」 東改札口</p>
          <p>↓徒歩2分</p>
          <p><span class="toei-bus">都営バス</span> 門33 「とうきょうスカイツリー駅入口」乗車</p>
          <p>↓豊海水産埠頭行き　（14駅）</p>
          <p><span class="toei-bus">都営バス</span> 門33 「深川二丁目」下車</p>
        </div>
      </section>
      <section id="sumida_kinshichou02"  class="data-wrap">
        <h3 class="sub-title">錦糸町駅からお越しの方</h3>
        <p class="summary">「錦糸町駅」南口より徒歩1分、都営バス東22 東京駅丸の内北口行き「錦糸町駅前」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
        <div class="data">
          <p><span>乗車時間</span>26分</p>
          <p><span>所要時間</span>33分</p>
        </div>
        <div class="route">
          <p>「錦糸町駅」南口</p>
          <p>↓徒歩1分</p>
          <p><span class="toei-bus">都営バス</span> 東22 「錦糸町駅前」乗車</p>
          <p>↓東京駅丸の内北口行き （17駅）</p>
          <p><span class="toei-bus">都営バス</span> 東22 「門前仲町」下車</p>
        </div>
      </section>

      <h2 class="title by-car">車でお越しの方</h2>
      <section id="sumida_car" class="data-wrap">
        <h3 class="sub-title">東京都墨田区からの所要時間</h3>
        <p class="data">
          両国駅から約12分<br>
          錦糸町駅から約14分<br>
          押上駅から約18分
        </p>
      </section>

      <h2 class="title">その他の区からのアクセス</h2>
      <section class="data-wrap">
        <div class="sec-route__links">
          <a class="item" href="/access/from-edogawa-ku/">江戸川区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chuo-ku/">中央区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chiyoda-ku/">千代田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-minato-ku/">港区からの<br class="only-sp">アクセス</a>
        </div>
      </section>

    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>