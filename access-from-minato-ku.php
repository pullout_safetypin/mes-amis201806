<?php /* Template Name: access-from-minato-ku */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access-sub.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section id="minato">
      <div class="nav-wrap">
        <h1 class="title only-sp"><span>港区からのアクセス</span></h1>
        <picture class="image">
          <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-minato-sp.png" media="(max-width:768px)">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-minato.png" alt="">
        </picture>
        <nav class="sec-links">
          <h1 class="title only-pc"><span>港区からのアクセス</span></h1>
          <div class="by-train">電車でお越しの方</div>
          <ul class="link-list">
            <li class="item"><a href="#minato_daimon01">大門駅から</a></li>
            <li class="item"><a href="#minato_shinbashi01">新橋駅から</a></li>
            <li class="item"><a href="#minato_tameike01">溜池山王駅から</a></li>
            <li class="item"><a href="#minato_sengakuji01">泉岳寺駅から</a></li>
            <li class="item"><a href="#minato_shinagawa01">品川駅から</a></li>
            <li class="item"><a href="#minato_roppongi01">六本木駅から</a></li>
            <li class="item"><a href="#minato_aoyama01">青山一丁目駅から</a></li>
            <li class="item"><a href="#minato_omotesandou01">表参道駅から</a></li>
            <li class="item"><a href="#minato_daiba01">台場駅から</a></li>
          </ul>
          <div class="by-car">車でお越しの方</div>
          <ul class="link-list">
            <li class="item"><a href="#minato_car">新橋駅から</a></li>
            <li class="item"><a href="#minato_car">浜松町駅から</a></li>
            <li class="item"><a href="#minato_car">青山一丁目駅から</a></li>
            <li class="item"><a href="#minato_car">品川駅から</a></li>
          </ul>
        </nav>
      </div>

      <h2 class="title by-train">電車でお越しの方</h2>
      <section id="minato_daimon01"  class="data-wrap">
        <h3 class="sub-title">大門駅からお越しの方<br class="only-sp">（乗り入れ線：都営浅草線、都営大江戸線）</h3>
        <p class="summary">都営大江戸線「大門駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>11分</p>
          <p><span>所要時間</span>18分</p>
        </div> <div class="route">
          <p><span class="ooedo">都営大江戸線</span>「大門駅」乗車</p>
          <p>↓両国・春日方面（5駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="minato_shinbashi01"  class="data-wrap">
        <h3 class="sub-title">新橋駅からお越しの方<br class="only-sp">（乗り入れ線： JR京浜東北線、JR東海道本線、JR山手線、JR横須賀線、東京メトロ銀座線、都営浅草線、ゆりかもめ線）</h3>
        <p class="summary">東京メトロ銀座線「新橋駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>14分</p>
          <p><span>所要時間</span>21分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="ginza">銀座線</span>「新橋駅」乗車</p>
          <p>↓浅草行き（3駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町」下車</p>
        </div>
      </section>
      <section id="minato_tameike01"  class="data-wrap">
        <h3 class="sub-title">溜池山王駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ銀座線、東京メトロ南北線）</h3>
        <p class="summary">東京メトロ銀座線「溜池山王駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>15分</p>
          <p><span>所要時間</span>22分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="ginza">銀座線</span>「溜池山王駅」乗車</p>
          <p>↓浅草行き（5駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="minato_sengakuji01"  class="data-wrap">
        <h3 class="sub-title">泉岳寺駅からお越しの方<br class="only-sp">（乗り入れ線：京急本線、都営浅草線）</h3>
        <p class="summary">都営浅草線「泉岳寺駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>17分</p>
          <p><span>所要時間</span>24分</p>
        </div> <div class="route">
          <p><span class="asakusa">都営浅草線</span>「泉岳寺駅」乗車</p>
          <p>↓印西牧の原行き（6駅）</p>
          <p><span class="asakusa">都営浅草線</span>「日本橋駅」下車</p>
          <p><span class="tozai">東京メトロ東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p><span class="tozai">東京メトロ東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="minato_shinagawa01"  class="data-wrap">
        <h3 class="sub-title">品川駅からお越しの方<br class="only-sp">（乗り入れ線：京急本線、JR東海道新幹線、JR京浜東北線、JR東海道本線、JR山手線、JR横須賀線）</h3>
        <p class="summary">JR東海道本線「品川駅」より「新橋駅」下車、東京メトロ銀座線「新橋駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>2回</p>
          <p><span>乗車時間</span>18分</p>
          <p><span>所要時間</span>25分</p>
        </div> <div class="route">
          <p>JR東海道本線「品川駅」乗車</p>
          <p>↓宇都宮行き（1駅）</p>
          <p>JR東海道本線「新橋駅」下車</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「新橋駅」乗車</p>
          <p>↓浅草行き（3駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="minato_roppongi01"  class="data-wrap">
        <h3 class="sub-title">六本木駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ日比谷線、都営大江戸線）</h3>
        <p class="summary">都営大江戸線「六本木駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>18分</p>
          <p><span>所要時間</span>25分</p>
        </div> <div class="route">
          <p><span class="ooedo">都営大江戸線</span>「六本木駅」乗車</p>
          <p>↓大門・両国方面（8駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="minato_aoyama01"  class="data-wrap">
        <h3 class="sub-title">青山一丁目駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ銀座線、東京メトロ半蔵門線、都営大江戸線）</h3>
        <p class="summary">東京メトロ半蔵門線「青山一丁目駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>19分</p>
          <p><span>所要時間</span>29分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「青山一丁目駅」乗車</p>
          <p>↓押上行き（8駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>
      <section id="minato_omotesandou01"  class="data-wrap">
        <h3 class="sub-title">表参道駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ銀座線、東京メトロ千代田線、東京メトロ半蔵門線）</h3>
        <p class="summary">東京メトロ半蔵門線「表参道駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>21分</p>
          <p><span>所要時間</span>31分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「表参道駅」乗車</p>
          <p>↓押上行き（9駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>
      <section id="minato_daiba01"  class="data-wrap">
        <h3 class="sub-title">台場駅からお越しの方<br class="only-sp">（乗り入れ線：ゆりかもめ線）</h3>
        <p class="summary">ゆりかもめ線「台場駅」より「豊洲駅」下車、東京メトロ有楽町線「豊洲駅」より「月島駅」下車、都営大江戸線「月島駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>2回</p>
          <p><span>乗車時間</span>28分</p>
          <p><span>所要時間</span>35分</p>
        </div> <div class="route">
          <p>ゆりかもめ線「台場駅」乗車</p>
          <p>↓豊洲行き（9駅）</p>
          <p>ゆりかもめ線「豊洲駅」下車</p>
          <p>東京メトロ<span class="yuurakuchou">有楽町線</span>「豊洲駅」乗車</p>
          <p>↓川越市行き（1駅）</p>
          <p>東京メトロ<span class="yuurakuchou">有楽町線</span>「月島駅」下車</p>
          <p><span class="ooedo">都営大江戸線</span>「月島駅」乗車</p>
          <p>↓両国・春日方面（1駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「「門前仲町駅」下車</p>
        </div>
      </section>

      <h2 class="title by-car">車でお越しの方</h2>
      <section id="minato_car"  class="data-wrap">
        <h3 class="sub-title">東京都港区からの所要時間</h3>
        <p class="data">
          新橋駅から約16分<br>
          浜松町駅から約19分<br>
          青山一丁目駅から約20分<br>
          品川駅から約26分　首都高速1号羽田線利用
        </p>
      </section>

      <h2 class="title">その他の区からのアクセス</h2>
      <section class="data-wrap">
        <div class="sec-route__links">
          <a class="item" href="/access/from-edogawa-ku/">江戸川区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-sumida-ku/">墨田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chuo-ku/">中央区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chiyoda-ku/">千代田区からの<br class="only-sp">アクセス</a>
        </div>
      </section>

    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>