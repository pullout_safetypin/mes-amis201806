<?php /* Template Name: access-from-edogawa-ku */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access-sub.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section id="edogawa">
      <div class="nav-wrap">
        <h1 class="title only-sp"><span>江戸川区からのアクセス</span></h1>
        <picture class="image">
          <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-edogawa-sp.png" media="(max-width:768px)">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-edogawa.png" alt="">
        </picture>
        <div>
          <nav class="sec-links">
            <h1 class="title only-pc"><span>江戸川区からのアクセス</span></h1>
            <div class="by-train">電車でお越しの方</div>
            <ul class="link-list">
              <li class="item"><a href="#edogawa-nishikasai">西葛西駅から</a></li>
              <li class="item"><a href="#edogawa-funabashi">船堀駅から</a></li>
            </ul>
            <div class="by-car">車でお越しの方</div>
            <ul class="link-list">
              <li class="item"><a href="#edogawa-edogawa_car">西葛西駅から</a></li>
              <li class="item"><a href="#edogawa-edogawa_car">船堀駅から</a></li>
              <li class="item"><a href="#edogawa-edogawa_car">葛西駅から</a></li>
              <li class="item"><a href="#edogawa-edogawa_car">篠崎駅から</a></li>
              <li class="item"><a href="#edogawa-edogawa_car">小岩駅から</a></li>
            </ul>
          </nav>
        </div>
      </div>

      <h2 class="title by-train">電車でお越しの方</h2>
      <section id="edogawa-nishikasai" class="data-wrap">
        <h3 class="sub-title">西葛西駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ東西線）</h3>
        <p class="summary">東京メトロ東西線「西葛西駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>10分</p>
          <p><span>所要時間</span>17分</p>
        </div>
        <div class="route">
          <p>東京メトロ<span class="tozai">東西線</span>「西葛西駅」乗車</p>
          <p>↓中野行き（4駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>

      <section id="edogawa-funabashi" class="data-wrap">
        <h3 class="sub-title">船堀駅からお越しの方<br class="only-sp">（乗り入れ線：都営新宿線）</h3>
        <p class="summary">都営新宿線「船堀駅」より「住吉駅」下車、東京メトロ半蔵門線「住吉駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>16分</p>
          <p><span>所要時間</span>26分</p>
        </div>
        <div  class="route">
          <p><span class="toei-shinjyuku">都営新宿線</span>「船堀駅」乗車</p>
          <p>↓橋本行き（4駅）</p>
          <p><span class="toei-shinjyuku">都営新宿線</span>「住吉駅」下車</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「住吉駅」乗車</p>
          <p>↓中央林間行き（1駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>

      <h2 class="title by-car">車でお越しの方</h2>
      <section id="edogawa-edogawa_car" class="data-wrap">
        <h3 class="sub-title">東京都江戸川区からの所要時間</h3>
        <p class="data">
          船堀駅から約20分<br>
          西葛西駅から約20分<br>
          葛西駅から約21分<br>
          篠崎駅から約27分<br>
          小岩駅から約32分
        </p>
      </section>

      <h2 class="title">その他の区からのアクセス</h2>
      <section class="data-wrap">
        <div class="sec-route__links">
          <a class="item" href="/access/from-sumida-ku/">墨田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chuo-ku/">中央区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chiyoda-ku/">千代田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-minato-ku/">港区からの<br class="only-sp">アクセス</a>
        </div>
      </section>
    </section>

  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>