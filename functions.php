<?php
add_theme_support( 'post-thumbnails' );

/**
 * hide author
 */
add_action( 'parse_query', function (){
  if ( is_author() ) {
    wp_redirect( home_url('/404.php') );
    exit;
  }
  elseif ( ($_SERVER['REQUEST_URI'] != '/404.php')  && is_404() ) {
    wp_redirect( home_url('/404.php') );
    exit;
  }
} );