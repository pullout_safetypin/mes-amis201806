<?php
$description;
$title;
$ogp_url;
//set values
if($post->post_type == 'page'){
  if($post->post_name == 'top'){
    $description = '「MES AMIS BALLET STUDIO」は東京都 江東区のバレエスタジオです。膝に負担がかからないよう、独特の工法で施工されたバレエ専門床。約100㎡の広々とした明るい環境です。中央区 千代田区 墨田区 江戸川区 港区からも通っていただけます。';
    $title = 'MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/';
  }
  else if($post->post_name == 'about'){
    $description = 'MES AMISとはフランス語で「仲間」という意味です。クラシックバレエを通して、様々な目標に向かって努力をする精神力、自尊心、感謝の気持や愛する心を育み、将来人間性豊かな人材に育つよう教育していきます。このスタジオから、人生にかけがえのない「経験」や「仲間」が沢山できる事を願っています。';
    $title = 'メザミについて - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/about/';
  }
  else if($post->post_name == 'teacher'){
    $description = 'スタジオでは国内外での豊富な経験とバレエへの愛情を持つ講師が熱意をもって指導をおこなっています。技術面だけでなく総合的にダンサーをサポートするため、アスリートに最適な食プログラムを提供する『アスリートフードマイスター資格』、バレエダンサー向けの筋力トレーニングである『プログレッシブ・バレエ・テクニック(PBT)』を修得しています。';
    $title = '講師の紹介 - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://shimokitahostel.com/teacher/';
  }
  else if($post->post_name == 'classes'){
    $description = 'メザミバレエスタジオでは3歳から入会できる5段階の月謝制クラスから、選抜式プロ養成クラス、大人クラスまで、実力、目標に合わせたクラスをご用意しています。また、国内/国外から講師を招いてのイベントも積極的に開催しています。';
    $title = 'クラスの紹介 - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/classes/';
  }
  else if($post->post_name == 'event'){
    $description = '「MES AMIS BALLET STUDIO」は東京都 江東区のバレエスタジオです。膝に負担がかからないよう、独特の工法で施工されたバレエ専門床。約100㎡の広々とした明るい環境です。MES AMISとはフランス語で「仲間」という意味です。クラシックバレエを通して、様々な目標に向かって努力をする精神力、自尊心、感謝の気持や愛する心を育み、将来人間性豊かな人材に育つよう教育していきます。このスタジオから、人生にかけがえのない「経験」や「仲間」が沢山できる事を願っています。中央区 千代田区 墨田区 江戸川区 港区からも通っていただけます。';
    $title = 'イベント - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/event/';
  }
  else if($post->post_name == 'schedule'){
    $description = 'こちらからスケジュールの詳細をご確認の上、お間違え無いようお越しください。';
    $title = 'スケジュール - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/schedule/';
  }
  else if($post->post_name == 'contact'){
    $description = '「MES AMIS BALLET STUDIO」への体験申込/お問い合わせはこちらから';
    $title = 'お問い合わせ - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/contact/';
  }
  else if($post->post_name == 'qa'){
    $description = '月謝クラスのよくあるご質問にお答えします。';
    $title = 'よくある質問 - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/qa/';
  }
  else if($post->post_name == 'news'){
    $description = '「MES AMIS BALLET STUDIO」は東京都 江東区のバレエスタジオです。膝に負担がかからないよう、独特の工法で施工されたバレエ専門床。約100㎡の広々とした明るい環境です。MES AMISとはフランス語で「仲間」という意味です。クラシックバレエを通して、様々な目標に向かって努力をする精神力、自尊心、感謝の気持や愛する心を育み、将来人間性豊かな人材に育つよう教育していきます。このスタジオから、人生にかけがえのない「経験」や「仲間」が沢山できる事を願っています。中央区 千代田区 墨田区 江戸川区 港区からも通っていただけます。';
    $title = 'お知らせ - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/news/';
  }
  else if($post->post_name == 'access'){
    $description = 'メザミバレエスタジオは門前仲町駅から徒歩7分、清澄白河駅から徒歩10分。中央区、千代田区、墨田区、江戸川区、港区からも通っていただけます。';
    $title = 'スタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else if($post->post_name == 'from-chiyoda-ku'){
    $description = '千代田区からメザミバレエスタジオへのアクセス';
    $title = '千代田区からメザミバレエスタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else if($post->post_name == 'from-edogawa-ku'){
    $description = '江戸川区からメザミバレエスタジオへのアクセス';
    $title = '江戸川区からメザミバレエスタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else if($post->post_name == 'from-chuo-ku'){
    $description = '中央区からメザミバレエスタジオへのアクセス';
    $title = '中央区からメザミバレエスタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else if($post->post_name == 'from-minato-ku'){
    $description = '港区からメザミバレエスタジオへのアクセス';
    $title = '港区からメザミバレエスタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else if($post->post_name == 'from-sumida-ku'){
    $description = '墨田区からメザミバレエスタジオへのアクセス';
    $title = '墨田区からメザミバレエスタジオへのアクセス - MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/access/';
  }
  else{
    $description = '「MES AMIS BALLET STUDIO」は東京都 江東区のバレエスタジオです。膝に負担がかからないよう、独特の工法で施工されたバレエ専門床。約100㎡の広々とした明るい環境です。MES AMISとはフランス語で「仲間」という意味です。クラシックバレエを通して、様々な目標に向かって努力をする精神力、自尊心、感謝の気持や愛する心を育み、将来人間性豊かな人材に育つよう教育していきます。このスタジオから、人生にかけがえのない「経験」や「仲間」が沢山できる事を願っています。中央区 千代田区 墨田区 江戸川区 港区からも通っていただけます。';
    $title = 'MES AMIS BALLET STUDIO';
    $ogp_url = 'http://m-amis-ballet.jp/';
  }
}
else if($post->post_type == 'post'){
    $description = $post->post_title;
    $title = $post->post_title.' - MES AMIS BALLET STUDIO';
    $ogp_url = post_permalink();
}
else{
  //Do something
}
?>
<!DOCTYPE html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="<?php echo $description ?>" />
  <meta property="og:title" content="<?php echo $title ?>" />
  <meta property="og:description" content="<?php echo $description ?>">
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo $ogp_url ?>" />
  <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/common/ogp.png" />
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="630">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="<?php echo $title ?>">
  <meta name="twitter:description" content="<?php echo $description ?>">
  <meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/common/ogp.png">
  <title><?php echo $title ?></title>
  <!-- favicon -->
  <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/common/favicon.ico">
  <!-- *** stylesheet *** -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/common.css?20241128" type="text/css">
  <!-- *** javascript *** -->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.cookie.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/common.js?v=20210813"></script>
