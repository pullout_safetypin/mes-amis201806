<?php /* Template Name: teacher */ ?>
<?php get_header(); ?>
<!-- local style and javascript -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/teacher.css?20241128" type="text/css">
</head>

<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="top_part">
    <div class="wrapper">
      <section>
        <h1 class="main-title"><span>講師の紹介</span></h1>
        <p>スタジオでは国内外での豊富な経験とバレエへの愛情を持つ講師が熱意をもって指導をおこなっています。</p>
        <p>
          技術面だけでなく総合的にダンサーをサポートするため、アスリートに最適な食プログラムを提供する『アスリートフードマイスター資格』、バレエダンサー向けの筋力トレーニングである『プログレッシブ・バレエ・テクニック(PBT)』を修得しています。
        </p>
        <div id="logos">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/afm_emblem_black_silver.png">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/11078500.jpg">
        </div>
      </section>
      <section>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/teacher_top.jpg">
      </section>
    </div>
  </section>
  <section id="contents">
    <section class="award">
      <h2 class="content-title"><span>指導者としての受賞歴</span></h2>
      <section class="award__body">
        <div class="award__lists">
          <ul>
            <li>
              <h3>Japan Ballet Competition 東京</h3>
              <ul>
                <li>2023 優秀指導者賞</li>
                <li>2022 最優秀指導者賞</li>
                <li>2022 優秀指導者特別賞</li>
                <li>2021 最優秀指導者賞</li>
                <li>2021 最優秀指導者特別賞</li>
                <li>2021秋 最優秀指導者賞</li>
                <li>2021秋 優秀指導者特別賞</li>
                <li>2020秋 優秀指導者賞</li>
                <li>2019秋 最優秀指導者賞</li>
                <li>2019 最優秀指導者賞</li>
                <li>2018 優秀指導者賞</li>
                <li>2017 最優秀指導者賞</li>
              </ul>
            </li>            
            <li>
              <h3>Japan Ballet Competition 愛知</h3>
              <ul>
                <li>2018 最優秀指導者賞</li>
                <li>初春2018 優秀指導者賞</li>
              </ul>
            </li>            
            <li>
              <h3>Japan Ballet Competition Grand Champtionships</h3>
              <ul>
                <li>2019 最優秀指導者賞</li>
                <li>2018 最優秀指導者賞</li>
              </ul>
            </li>            
          </ul>
        </div>
        <div class="award__lists">
          <ul>
            <li>
              <h3>YBCバレエコンクール東京</h3>
              <ul>
                <li>2023 優秀指導者賞</li>
              </ul>
            </li>
            <li>
              <h3>Victoire Ballet Competition Tokyo</h3>
              <ul>
                <li>2022秋 コンテンポラリー・シニアの部 優秀指導者賞</li>
                <li>2022秋 コンテンポラリー・ジュニアの部 優秀指導者賞</li>
              </ul>              
            </li>
            <li>
              <h3>New Ballet Competition</h3>
              <ul>
                <li>2021 最優秀指導者賞</li>
              </ul>              
            </li>
            <li>
              <h3>JBC PREPARETION 東京</h3>
              <ul>
                <li>2019 優秀指導者賞</li>
              </ul>              
            </li>
            <li>
              <h3>エスポワール全国バレエコンクール</h3>
              <ul>
                <li>2019 コンクール部門ジュニアの部 指導者賞</li>
                <li>2017 コンクール部門シニアの部 指導者賞</li>
              </ul>
            </li>
            <li>
              <h3>チャリティーバレエコンクール</h3>
              <ul>
                <li>第4回 プレコンB部門 優秀指導者賞</li>
              </ul>              
            </li>
          </ul>
        </div>
      </section>
    </section>

    <section class="teacher">
      <h2 class="content-title"><span>講師プロフィール</span></h2>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/ami.sakachi.jpg" alt=""></span>
          <h3>坂地 亜美 / <span>Ami Sakachi</span></h3>
          <p>主宰講師 / クラシック</p>
        </div>
        <div>
          <p>4歳よりバレエを始め、10歳よりソウダバレエスクール及バレエスタジオミューズにて、宗田静子、夏山周久、原田高博に師事。</p>
          <p>同校にで海外よりゲストとして招かれた著名ダンサーや講師達からも世界各国のバレエメソッドを学び強い影響を受ける。</p>
          <p>13歳でロシア ワガノワバレエ学校に短期留学、リュドミラ コワリョーワに師事。</p>
          <p>15歳でローザンヌ国際バレエコンクールに於いて、スカラーシップ賞を受賞しイギリスのイングリッシュ・ナショナル・バレエスクールに２年留学。2003年 K-BALLET COMPANY に入団、活躍。</p>
          <p>2004年より再度海外へ渡り、ポルトガル国立バレエ団、オランダ国立バレエ団、香港バレエ団にて活躍。ピーター・ライト、ローラン・プティ、ハンス・ヴァンマネン、ナチョ・ドゥアトなど数々の有名な振付家の作品をはじめ、古典からコンテンポラリーまで幅広いレパートリーのソロ、パドドゥを踊る。</p>
          <p>2012年、香港バレエ団を退団。香港バレエ団主催による子供教育プログラムでのゲスト講師や、数々のバレエスクールにて指導に専念。</p>
          <p>2014年5月　日本帰国。東京を拠点に、長年のプロフェッショナル生活で得たバレエ知識と国際経験を生かし子供から大人まで後進の指導にあたる。</p>
          <p>2016年8月　メザミバレエスタジオ設立。</p>
          <h3>受賞歴</h3>
          <p>2000年 ローザンヌ国際バレエコンクール／決選　スカラーシップ賞</p>
          <p>2003年 ユースアメリカグランプリ／ニューヨーク決選 第3位</p>
          <p>こうべ全国洋舞コンクール　第1位</p>
          <p>NBA全国バレエコンクール　クラシック部門第3位　モダン部門第2位 及び ベストアーティスト賞　同時受賞</p>
          <p>全国コンクール in Nagoya 第3位</p>
          <!-- <h3>指導者としての受賞歴</h3>
          <p>Japan Ballet Competition 東京2017 最優秀指導者賞</p>
          <p>第3回エスポワール全国バレエコンクール 指導者賞</p>
          <p>Japan Ballet Competition 愛知新春2018 優秀指導者賞</p> -->
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/yo.takahira.jpg" alt=""></span>
          <h3>高比良 洋 / <span>Yo Takahira</span></h3>
          <p>主宰講師 / クラシック、コンテンポラリー</p>
        </div>
        <div>
          <p>4歳より横山慶子、横山真理のもとで現代舞踊を学ぶ。</p>
          <p>19歳よりバレエを始め余バレエアカデミーにて余芳美に師事。</p>
          <p>2002年より中国国立北京舞踊学校付属中学に留学、曹錦栄に師事。同校卒業と同時に中国国立バレエ団に入団、国内ツアー等に参加。</p>
          <p>2005年に香港バレエ団に入団。2012年ソリストに昇進。</p>
          <p>2014年Jacob’s Pillow Dance Festival 2014”に参加し、ナチョ・デュアト氏の作品 『Castrati』のメインを踊り好評を得る。2014年6月同バレエ団を退団。同6月よりフリーランスダンサー兼講師として活動を始める。</p>
          <p>平成27年度日本バレエ協会『合同バレエの夕べ』にて関東支部作品『The Swan』にて主演。2015年Architanz主催の『STRAVINSKY TRIPLE BILL』においでウヴェ ショルツ振付け『春の祭典』ソロ版を日本人として初めて上演し好評を得る。同年『The Dance Times』が選ぶダンサー月間ベスト10に選ばれる。</p>
          <p>2016年より 全国各地で年7回開催される「Japan Ballet Competion」において、レギュラー審査員として招聘されている。</p>
          <p>2016年 第2回 エスポワール全国バレエコンクール においても審査員を務める。</p>
          <p>2016年8月　メザミバレエスタジオ設立。</p>
          <h3>受賞歴</h3>
          <p>東京新聞バレエコンクール／現代舞踊部門ジュニア部 第3位</p>
          <p>埼玉全国舞踊コンクール／現代舞踊部門ジュニア部 第2位</p>
          <p>神戸全国洋舞コンクール／現代舞踊部門ジュニア部 第2位</p>
          <p>板橋区全国洋舞コンクール／現代舞踊部門ジュニア部 及び 舞踊部門第2部 第1位</p>
          <!-- <h3>指導者としての受賞歴</h3>
          <p>Japan Ballet Competition 東京2017 最優秀指導者賞</p>
          <p>第3回エスポワール全国バレエコンクール 指導者賞</p>
          <p>Japan Ballet Competition 愛知新春2018 優秀指導者賞</p> -->
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/asaka.honma.jpg" alt=""></span>
          <h3>本馬 亜紗花 / <span>Asaka Honma</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>4歳よりバレエを始める。</p>
          <p>12歳よりソウダバレエスクールにて宗田静子に師事。</p>
          <p>2003年カナダ、スクールオブアルバータバレエに入学。</p>
          <p>2007年6月Pre-Proffessionalコース修了。8月アルバータバレエ団入団。</p>
          <p>2010年バンクーバーオリンピック開会式典にて踊る。</p>
          <p>2011年服部有吉主催ステップ1にてゲスト出演。所属中、様々な主要キャストを踊る。</p>
          <p>2015年3月、服部有吉作『カルメン』主役カルメンを踊り退団。</p>
          <p>2015年5月より様々なバレエスタジオにて指導を行う。</p>
          <p>真夏の夜の夢 デミソリスト妖精／眠れる森の美女 フロリナ王女、勇気の精／白鳥の湖 パドトロワ、二羽の白鳥、四羽の白鳥／オセロ エミリア／くるみ割り人形／金平糖の精、クララ、中国の踊り、スペインの踊り／カルメン カルメン／バランシン作品 多数出演／アルバータバレエ監督作品 多数出演</p>
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/nanako.takeuchi.jpg" alt=""></span>
          <h3>竹内 菜那子 / <span>Nanako Takeuchi</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>3歳よりクラシックバレエを始める。</p>
          <p>2008年英国Royal Academy of Danceに短期留学。2010年バレエコンクールin横浜コンテンポラリー部門第1位。FLAP全国バレエコンクールクラシック部門入賞第2位。つくば国際バレエコンクールモダン部門最優秀賞金賞。2011年全国鎌倉バレエコンクールクラシック部門第4位。バレエコンクールin横浜クラシック部門奨励賞第3位。2012年つくば洋舞コンクールクラシック部門第1位コンテンポラリー部門第1位、茨城県教育長賞、最優秀賞。2013年全国舞踊コンクール現代舞踊部門ファイナリスト。埼玉全国舞踊コンクールモダンダンス部門奨励賞。全日本バレエコンクールファイナリスト。</p>
          <p>2014年谷桃子バレエ団に準団員として入団。「くるみ割り人形」葦笛を踊る。2015年正団員に昇格。地方巡回公演で主役を務める。2016年「眠れる森の美女」フロリナ姫、2017年「ドン・キホーテ」ジャネッタなど各作品の主要パートを踊り、ソリストに昇格。2018年「白鳥の湖」オデット/オディールで全幕主役デビュー、ファーストソリストに昇格。</p>
          <p>団外では、松竹株式会社「滝沢歌舞伎」に出演。チャコット株式会社「バレエ・プリンセス」で宝石を踊る。「CEATEC JAPAN」SHARP 8K AQUOS TVプレゼンテーションでバレエパフォーマンスを行う。日本バレエ協会、都民バレエフェスティバル「ライモンダ」エンリエット、グラン・パ・クラシックを踊る。</p>
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/yuko.kimra.jpg" alt=""></span>
          <h3>木村 優子 / <span>Yuko Kimura</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>4歳よりバレエを始める。</p>
          <p>12歳より高橋洋美に師事。</p>
          <p>2013年　東京アートブリッジコンクール　高校生の部1位</p>
          <p>2014年4月　日本大学芸術学部演劇学科洋舞コース入学</p>
          <p>2014年9月　新国立劇場バレエ団入団</p>
          <p>以来、アーティスト契約ダンサーとして、ソリスト役を含むほぼ全ての作品に出演。</p>
          <p>2020年　GYROKINESIS®アプレンティス取得</p>
          <p>2023年　GYROKINESIS®認定トレーナー取得</p>
          <p>現在、バレエ団での活動の傍ら、各地のバレエスタジオにて子供から大人まで後進の指導にあたる。</p>
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/chieri.kitamura.jpg" alt=""></span>
          <h3>北村 桜桃 / <span>Chieri Kitamura</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>フジタバレエ研究所にて2歳よりバレエを始める。藤田美知子に師事。</p>
          <p>2012年JR西日本入社。総合職として勤務したのち退職。</p>
          <p>2013年NBAバレエ団入団。主なレパートリーに「くるみ割り人形」コロンビーヌなど。全ての公演に出演。</p>
          <p>2023年NBAバレエ団退団。</p>
          <p>現在NBAバレエ団附属バレエ学校担当講師。子供から大人までバレエを指導。</p>
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/mebae.niki.jpg" alt=""></span>
          <h3>仁木 芽映 / <span>Mebae Niki</span></h3>
          <p>臨時講師 / クラシック</p>
        </div>
        <div>
          <p>4歳よりバレエを始める。</p>
          <p>トゥールバレエスタジオにて遠藤真弓に師事。</p>
          <p>2016年JoffreyBalletSchoolに留学。</p>
          <p>BESJ公認マットピラティスの資格を取得しピラティスのインストラクターとしても活動中。</p>
          <p>大学では日本大学芸術学部にてコンテンポラリーダンス、モダンバレエ、日本舞踊など様々なジャンルの踊りを学び、<br class="only-pc">卒業後はフリーダンサーとして日本バレエ協会等の公演に出演しつつ、BESJ公認マットピラティスの資格を取得しピラティスのインストラクターとしても活動中。</p>
        </div>
      </section>

    </section>

    <section class="teacher">
      <h2 class="content-title"><span>ゲスト講師プロフィール</span></h2>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/asako.terada.jpg" alt=""></span>
          <h3>寺田 亜沙子 / <span>Asako Terada</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>大阪府出身。1998年より鮫島バレエ教室にて鮫島洋子に師事。</p>
          <p>03年より新国立劇場バレエ研修所に第二期生として入所。05年に修了し、同年秋より新国立劇場バレエ団に入団。09年には『ドン・キホーテ』で主役デビューを果たし、その後もアシュトン『シンデレラ』の主役、W.イーグリング版『眠れる森の美女』リラの精をはじめ、バランシン、マクミラン、ビントレー作品などでも主要な役を踊っている。10年ソリスト、13年ファースト・ソリストに昇格。</p>
        </div>
      </section>
      <section>
        <div>
          <span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/teacher/ayumi.shiraishi.jpg" alt=""></span>
          <h3>白石 あゆみ / <span>Ayumi Shiraishi</span></h3>
          <p>講師 / クラシック</p>
        </div>
        <div>
          <p>愛媛県生まれ。6歳よりバレエを始め、八束公子, マリーナ・コンドラチェーワに師事。</p>
          <p>1999年全日本バレエコンクールin Nagoya第1位。<br>
            2000年第14回全日本バレエコンクール入賞第3位。<br>
            2002～2005年ボリショイバレエ学校に留学。<br>
            2004年キエフ国際バレエコンクールファイナル出場 ジプロン受賞。<br>
            2005年ボリショイバレエ学校卒業証書授与。<br>
            2006年K-Ballet Companyに入団。2015年プリンシパルに昇格。</p>
          <p>主な主演作品は『カルメン』『白鳥の湖』『ドン・キホーテ』『クルミ割り人形』『シンデレラ』『ラ・バヤデール』『海賊』、プティ、バランシン、アシュトンなどの有名な作品も含め、その他数々のソリストを勤めた。</p>
          <p>K-Ballet School TTC(ティーチャーズ トレーニング コース)取得後小石川校で教師を勤める。2017年8月に退団後現在フリー。</p>
        </div>
      </section>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>

</html>