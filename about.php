<?php /* Template Name: about */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/about.css?20241128" type="text/css">
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="top_part">
    <div class="wrapper">
      <section>
        <h1 class="main-title"><span>メザミバレエスタジオの想い</span></h1>
        <p>MES AMISとはフランス語で「仲間」という意味です。</p>
        <p>クラシックバレエを通して、様々な目標に向かって努力をする精神力、自尊心、感謝の気持や愛する心を育み、将来人間性豊かな人材に育つよう教育していきます。
        <p>このスタジオから、人生にかけがえのない「経験」や「仲間」が沢山できる事を願っています。</p>
        <div id="signature">
          <span class="title">メザミバレエスタジオ代表</span>
          <div class="sign_body"><span class="ami"></span><span class="yo"></span></div>
        </div>
      </section>
      <section>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/about_top.jpg">
      </section>
    </div>
  </section>
  <section id="contents">
    <section id="about">
      <h2 class="content-title"><span>メザミバレエの6つの特徴</span></h2>
      <div>
        <div>
          <h3>1. 語学も学べる国際的バレエ</h3>
          <p>
            世界で活躍するバレエダンサーになるためには幼いころからの語学教育が重要です。
          </p>
          <p>
            海外でプロとして活躍してきた講師によるレッスンでは生きた英語が学べます。レッスンを通して学ぶ英語は、バレエと別で習うより効率的に習得できます。
          </p>
          <h3>2. 世界レベルの指導</h3>
          <p>
            両講師の海外でのプロ経験を基に世界レベルの水準で年齢や習熟度に応じたレッスンが行われます。
          </p>
          <p>
            高い芸術性を育むために、幼いころからのきめ細やかな指導、情操教育を行い、人格的にも世界に通用するダンサーへと育てます。
          </p>
          <h3>3. コンクール指導体制も充実</h3>
          <p>
            主宰の坂地はローザンヌ国際バレエコンクールで受賞経験を持ち、国内バレエコンクールでも数々入賞、ディレクターの高比良も同じく多くの受賞経験を持ち、数々のクラシックバレエコンクール審査員も務めています。
          </p>
          <p>
            両講師の経験を最大限に活かし、国内だけでなく、海外のコンクールへの挑戦、そしてプロダンサーへの夢をサポートします。<br>
            「さらに上達したい！」と思う生徒を全面的にバックアップできる体制が整っています。
          </p>
          <div class="btns">
            <a class="link-teacher-page" href="/teacher/">指導者としての受賞歴はこちら</a>
          </div>

          <h3>4. 怪我の防止対策</h3>
          <p>
            バレエを習っていると、時には怪我をしたりします。練習に熱が入れば入るほど、くせになったり悪化したりしがちです。<br>
            当スタジオでは常に生徒の状態を観察し、多くの有名バレエダンサーが通う接骨院、整骨院との提携を結び、安全にバレエのレッスンが続けられるよう、配慮しています。
          </p>
          <p>
            たばる治療院<br><a href="http://www.honetugitabaru.com/">http://www.honetugitabaru.com/</a><br>
            深川治療院<br><a href="http://5980.jp/">http://5980.jp/</a>
          </p>
          <p>
            スタジオの床は幼い子供たちの膝にも負担がかからないよう、独特の工法で施工をし、ダンサーの怪我を未然に防ぎます。
          </p>
          <h3>5. 「美」を追求する大人バレエ</h3>
          <p>
            「大人から習うバレエ」は全国的に増えており、健康だけでなく、美しい所作やたたずまい、女性としての品格が自然と身につきます。<br>
          </p>
          <p>
            初めてでも心配ありません。まずはストレッチクラスから始めてみませんか？丁寧に基本からお教えします。<br>
            昔バレエを習っていたけど。。。という方も大歓迎です。改めてバレエの楽しさを思い出しましょう。
          </p>
          <h3>6. 専門的な食生活サポート</h3>
          <p>
            バレエダンサーとして活躍するために、体調を整え、ウェイトをコントロールすることは成長期の子ども達には大変難しいものです。<br>
            行き過ぎた減量を繰り返すあまり、摂食障害を引き起こす事もよくあります。
          </p>
          <p>
            バレエダンサーとしての経験と、アスリートフードマイスター資格の知識で、健康で美しく、強い身体を作るアドバイスを行います。
          </p>
        </div>
        <div>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/about_right.jpg">
        </div>
      </div>
    </section>
    <section id="facility">
      <h2 class="content-title"><span>設備</span></h2>
      <div>
        <ul>
          <li>スタジオ 約100㎡</li>
          <li>天井高 約3m</li>
          <li>バレエ専用の特殊構造フロア</li>
          <li>冷暖房完備</li>
          <li>男女別 更衣室</li>
          <li>パウダースペース</li>
          <li>スタジオ内トイレ<br>(手洗いカウンター)</li>
          <li>講師/ゲストルーム</li>
          <li>2面鏡</li>
          <li>貴重品ロッカー</li>
          <li>バレエ雑誌&DVD 本棚</li>
          <li>冷蔵庫</li>
          <li>電子レンジ</li>           
          <li>ウォーターサーバー<br>(スポーツ飲料水無料)</li>
          <li>ヨガマット</li>
          <li>その他トレーニング器具 多数</li>
        </ul>
      </div>
    </section>
    <section id="access">
      <h2 class="content-title"><span>アクセス</span></h2>
      <div>
        <div>
          <h3>MES AMIS BALLET STUDIO <span>−メザミバレエスタジオ−</span></h3>
          <p>〒135-0033 東京都江東区深川2-26-8-1F<br>TEL：090-2440-2992</p>
          <ul class="dot">
            <li>門前仲町駅から：<br>徒歩7分</li>
            <li>清澄白河駅から：<br>徒歩10分</li>
            <li>都営バス 有明/豊洲方面から：<br>海01/門19系統 「門前仲町」下車 徒歩６分</li>
            <li>勝どき/月島/清澄白河/両国/押上/亀戸方面から：<br>門33系統「深川二丁目」下車 徒歩３分</li>
          </ul>
          <!--div class="btns"><a href="howtowalk.php">門前仲町駅からの歩き方</a></div-->
        </div>
        <div>
          <iframe class="gmap" style="border: 0;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6482.082256507514!2d139.79503443292663!3d35.67598928019551!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601889114ed7c6d5%3A0x6b417fe182e1f5b3!2z44CSMTM1LTAwMzMg5p2x5Lqs6YO95rGf5p2x5Yy65rex5bed77yS5LiB55uu77yS77yW4oiS77yY!5e0!3m2!1sja!2sjp!4v1467379666817" width="576" height="100%" frameborder="0"></iframe>
        </div>
      </div>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>