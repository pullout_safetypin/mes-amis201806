<?php get_header(); ?>
<!-- local js and css -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/single.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <!-- CONTENTS -->
  <div id="wrapper"> 
    <section id="contents">
      <?php if(have_posts()): while(have_posts()):the_post(); ?>
        <h1 class="post_title"><span><?php the_title(); ?></span></h1>
        <p>
          カテゴリ：<span><?php the_category(' '); ?></span>
          公開日：<span><?php echo get_post_time('Y年n月j日'); ?></span>
        </p>
        <div class="content_body"><?php the_content(); ?></div>
      <?php endwhile; endif; ?>
      <section class="pager">
      <span><?php previous_post_link('%link','前の記事'); ?></span>
      <span><a href="/blog/">一覧へ</a></span>
      <span><?php next_post_link('%link','次の記事'); ?></span>
      </section>
    </section>
    <!-- /CONTENTS -->
    <?php get_sidebar(); ?>
  </div>
  <footer>
  <?php get_footer(); ?>
  </footer>
</body>
</html>