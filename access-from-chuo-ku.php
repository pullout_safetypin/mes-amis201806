<?php /* Template Name: access-from-chuo-ku */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access-sub.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section>
      <section id="chuuou">
        <div class="nav-wrap">
          <h1 class="title only-sp"><span>中央区からのアクセス</span></h1>
          <picture class="image">
            <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-chuo-sp.png" media="(max-width:768px)">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-chuo.png" alt="">
          </picture>
          <div>
            <section class="sec-links">
            <h1 class="title only-pc"><span>中央区からのアクセス</span></h1>
              <div class="by-train">電車でお越しの方</div>
              <ul class="link-list">
                <li class="item"><a href="#chuuou_ginza01">銀座駅から</a></li>
                <li class="item"><a href="#chuuou_tsukishima01">月島駅から</a></li>
                <li class="item"><a href="#chuuou_tsukiji01">築地駅から</a></li>
              </ul>
              <div class="by-bus">バスでお越しの方</div>
              <ul class="link-list">
                <li class="item"><a href="#chuuou_tsukishima02">月島駅から</a></li>
                <li class="item"><a href="#chuuou_kachidoki02">勝どき駅から</a></li>
                <li class="item"><a href="#chuuou_kayabachou02">茅場町駅から</a></li>
                <li class="item"><a href="#chuuou_nihonbashi02">日本橋駅から</a></li>
                <li class="item"><a href="#chuuou_kinshichou02">錦糸町駅から</a></li>
              </ul>
              <div class="by-car">車でお越しの方</div>
              <ul class="link-list">
                <li class="item"><a href="#chuuou_car">銀座駅から</a></li>
                <li class="item"><a href="#chuuou_car">勝どき駅から</a></li>
                <li class="item"><a href="#chuuou_car">月島駅から</a></li>
                <li class="item"><a href="#chuuou_car">日本橋駅から</a></li>
              </ul>
            </section>
          </div>
        </div>

        <h2 class="title by-train">電車でお越しの方</h2>
        <section id="chuuou_ginza01"  class="data-wrap">
          <h3 class="sub-title">銀座駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ銀座線、東京メトロ日比谷線、東京メトロ丸ノ内線）</h3>
          <p class="summary">東京メトロ銀座線「銀座駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
          <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>9分</p>
          <p><span>所要時間</span>16分</p>
          </div> <div class="route">
          <p>東京メトロ<span class="ginza">銀座線</span>「銀座駅」乗車</p>
          <p>↓浅草行き（2駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
          </div>
        </section>
        <section id="chuuou_tsukishima01"  class="data-wrap">
          <h3 class="sub-title">月島駅よりお越しの方（乗り入れ線：都営大江戸線、有楽町線）</h3>
          <p class="summary">都営大江戸線「月島駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
          <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>1分</p>
          <p><span>所要時間</span>8分</p>
          </div> <div class="route">
          <p><span class="ooedo">都営大江戸線</span>「月島駅」乗車</p>
          <p>↓両国・春日方面（1駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「門前仲町駅」下車</p>
          </div>
        </section>
        <section id="chuuou_tsukiji01"  class="data-wrap">
          <h3 class="sub-title">築地駅よりお越しの方（乗り入れ線：東京メトロ日比谷線）</h3>
          <p class="summary">東京メトロ日比谷線「築地駅」より「茅場町駅」下車、東京メトロ東西線「茅場町駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
          <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>11分</p>
          <p><span>所要時間</span>18分</p>
          </div> <div class="route">
          <p>東京メトロ<span class="hibiya">日比谷線</span>「築地駅」乗車</p>
          <p>↓北越谷行き（2駅）</p>
          <p>東京メトロ<span class="hibiya">日比谷線</span>「茅場町駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「茅場町駅」乗車</p>
          <p>↓西船橋行き（1駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
          </div>
        </section>

        <h2>バスでお越しの方</h2>
        <section id="chuuou_tsukishima02"  class="data-wrap">
          <h3 class="sub-title">月島駅からお越しの方</h3>
          <p class="summary">「月島駅」2番出口より徒歩1分、都営バス門33 亀戸駅前行き「月島駅前」乗車、「深川二丁目」下車、メザミバレエスタジオ徒歩3分</p>
          <div class="data">
            <p><span>乗車時間</span>8分</p>
            <p><span>所要時間</span>12分</p>
          </div> <div class="route">
            <p>「月島駅」2番出口</p>
            <p>↓徒歩1分</p>
            <p><span class="toei-bus">都営バス</span> 門33「月島駅前」乗車</p>
            <p>↓亀戸駅前行き （4駅）</p>
            <p><span class="toei-bus">都営バス</span> 門33  「深川二丁目」下車</p>
          </div>
        </section>
        <section id="chuuou_kachidoki02"  class="data-wrap">
          <h3 class="sub-title">勝どき駅からお越しの方</h3>
          <p class="summary">「勝どき駅」A1番出口より徒歩1分、都営バス門33 亀戸駅前行き「勝どき駅前」乗車、「深川二丁目」下車、メザミバレエスタジオ徒歩3分</p>
          <div class="data">
            <p><span>乗車時間</span>12分</p>
            <p><span>所要時間</span>16分</p>
          </div> <div class="route">
            <p>「勝どき駅」A1番出口</p>
            <p>↓徒歩1分</p>
            <p><span class="toei-bus">都営バス</span> 門33 「勝どき駅前」乗車</p>
            <p>↓亀戸駅前行き　（6駅）</p>
            <p><span class="toei-bus">都営バス</span> 門33 「深川二丁目」下車</p>
          </div>
        </section>
        <section id="chuuou_kayabachou02"  class="data-wrap">
          <h3 class="sub-title">茅場町駅からお越しの方</h3>
          <p class="summary">「茅場町駅」4a出口より徒歩1分、都営バス東20 錦糸町駅前行き「茅場町」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
          <div class="data">
            <p><span>乗車時間</span>14分</p>
            <p><span>所要時間</span>21分</p>
          </div> <div class="route">
            <p>「茅場町駅」4a出口</p>
            <p>↓徒歩1分</p>
            <p><span class="toei-bus">都営バス</span> 東20 「茅場町」乗車</p>
            <p>↓錦糸町駅前行き　（5駅）</p>
            <p><span class="toei-bus">都営バス</span> 東20 「門前仲町」下車</p>
          </div>
        </section>
        <section id="chuuou_nihonbashi02"  class="data-wrap">
          <h3 class="sub-title">日本橋駅からお越しの方</h3>
          <p class="summary">「日本橋駅」B10出口より1分、都営バス東20 錦糸町駅前行き「日本橋」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
          <div class="data">
            <p><span>乗車時間</span>14分</p>
            <p><span>所要時間</span>21分</p>
          </div> <div class="route">
            <p>「日本橋」B10出口</p>
            <p>↓徒歩1分</p>
            <p><span class="toei-bus">都営バス</span> 東20 「日本橋」乗車</p>
            <p>↓錦糸町駅前行き （7駅）</p>
            <p><span class="toei-bus">都営バス</span> 東20 「門前仲町」下車</p>
          </div>
        </section>
        <section id="chuuou_kinshichou02"  class="data-wrap">
          <h3 class="sub-title">錦糸町駅からお越しの方</h3>
          <p class="summary">「錦糸町駅」南口より徒歩1分、都営バス東22 東京駅丸の内北口行き「錦糸町駅前」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
          <div class="data">
            <p><span>乗車時間</span>26分</p>
            <p><span>所要時間</span>33分</p>
          </div> <div class="route">
            <p>「錦糸町駅」南口</p>
            <p>↓徒歩1分</p>
            <p><span class="toei-bus">都営バス</span> 東22 「錦糸町駅前」乗車</p>
            <p>↓東京駅丸の内北口行き （17駅）</p>
            <p><span class="toei-bus">都営バス</span> 東22 「門前仲町」下車</p>
          </div>
        </section>

        <h2 class="title by-car">車でお越しの方</h2>
        <section id="chuuou_car" class="data-wrap">
          <h3 class="sub-title">東京都中央区からの所要時間</h3>
          <p class="data">
            銀座駅から約15分<br>
            勝どき駅から約10分<br>
            月島駅から約12分<br>
            日本橋駅から約10分
          </p>
        </section>

        <h2 class="title">その他の区からのアクセス</h2>
        <section class="data-wrap">
          <div class="sec-route__links">
            <a class="item" href="/access/from-edogawa-ku/">江戸川区からの<br class="only-sp">アクセス</a>
            <a class="item" href="/access/from-sumida-ku/">墨田区からの<br class="only-sp">アクセス</a>
            <a class="item" href="/access/from-chiyoda-ku/">千代田区からの<br class="only-sp">アクセス</a>
            <a class="item" href="/access/from-minato-ku/">港区からの<br class="only-sp">アクセス</a>
          </div>
        </section>

      </section>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>