<section id="sidebar">
  <!--section>
    <h3><span>CATEGORY</span></h3>
    <ul id="cate_list"-->
    <?php // wp_list_categories('title_li=&depth=1&hide_empty=1'); ?>
    <!--/ul>
  </section-->
  <section>
    <h3><span>LATEST POSTS</span></h3>
    <ul class="post_list">
    <?php
    $args = array(
      'posts_per_page' => 8,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'post_type' => 'post',
      'post_status' => 'publish',
      'category_name' => 'news'

    );
    $the_query = new WP_Query($args);
    if ( $the_query->have_posts() ) :
      while ( $the_query->have_posts() ) : $the_query->the_post();
    ?>
    <li <?php if( (date('Ymd') - get_post_time('Ymd')) < 14 ){echo('class="new"');} ?>>
      <a href="<?php the_permalink(); ?>">
        <div>
          <p class="release_date"><?php echo get_post_time('Y年n月j日'); ?></p>
          <p class="title"><?php the_title(); ?></p>
        </div>
        <?php
        if( has_post_thumbnail() ){
          the_post_thumbnail('thumbnail');
        }
        else{
          echo ('<img src="'.get_template_directory_uri().'/assets/img/common/icon_amisanyosan.jpg">');
        }
        ?>
      </a>
    </li>
    <?php endwhile; endif; ?>
    </ul>
    <a class="read_more" href="/blog/">SHOW ALL NEWS</a>
  </section>
</section>
