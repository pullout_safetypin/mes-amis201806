const fs = require('fs');
const pkg = JSON.parse(fs.readFileSync('./package.json'));
//const assetsPath = path.resolve(pkg.path.assetsDir);
const gulp = require('gulp');// gulp
const sass = require('gulp-sass');// sass compiler
const filter = require('gulp-filter');
const uglifyjs = require( 'gulp-uglify' );
const sourcemaps = require('gulp-sourcemaps');
const cleanCss  = require('gulp-clean-css');
// add vender prifix
const autoprefixer = require('gulp-autoprefixer');
const autoprefixer_option = {
  browsers: ['last 2 versions'],
  cascade: false
};
// error handling
const plumber = require('gulp-plumber');
//-------------------
gulp.task('sass', function() {
    gulp.src('./assets/sass/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init( { largeFile: true } ))
        .pipe(sass( { outputStyle: 'compressed' } ))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer(autoprefixer_option))
//        .pipe(cleanCss())
        .pipe(gulp.dest('../assets/css/'));
});
gulp.task('default', function() {
    gulp.watch('./assets/sass/**/*.scss',['sass']);
});