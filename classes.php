<?php /* Template Name: classes */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/classes.css?20241128" type="text/css">
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="top_part">
    <div class="wrapper">
      <section>
        <h1 class="main-title"><span>クラスの紹介</span></h1>
        <p>メザミバレエスタジオでは3歳から入会できる5段階の月謝制クラスから、選抜式プロ養成クラス、大人クラスまで、実力、目標に合わせたクラスをご用意しています。</p>
        <p>また、国内/国外から講師を招いてのイベントも積極的に開催しています。</p>
        <div class="btns">
          <a href="/schedule/">クラスのスケジュール</a>
              <?php
              $args = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'category_name' => 'event'
              );
              $the_query = new WP_Query($args);
              if ( $the_query->have_posts() ) :
                echo('<a href="/event/">イベント情報</a>');
              ?>
              <?php endif; ?>
        </div>
      </section>
      <section>
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/classes/classes_top.jpg">
      </section>
    </div>
  </section>
  <section id="contents">
    <section>
      <h2 class="content-title"><span>月謝クラス</span></h2>
      <section>
        <div>
          <table>
            <thead>
              <tr>
                <th>クラス名</th>
                <th>レベル</th>
                <th>対象者</th>
                <th>1クラスの時間</th>
                <th>月謝</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><span>バンビーノ</span></td>
                <td>入門</td>
                <td>3、4歳から</td>
                <td>45分</td>
                <td>週1回：¥7,500<br>週2回：¥11,500</td>
              </tr>
              <tr>
                <td><span>ジュニアC</span></td>
                <td>初級</td>
                <td>6、7歳から</td>
                <td>75分</td>
                <td>週1回：¥8,500<br>週2回：¥12,500<br>週3回：¥15,500<br>週4回：¥18,500</td>
              </tr>
              <tr>
                <td><span>ジュニアB</span></td>
                <td>初中級</td>
                <td>9、10歳から</td>
                <td rowspan="3">90分</td>
                <td rowspan="3">週1回：¥9,500<br>週2回：¥13,500<br>週3回：¥16,500<br>週4回：¥19,500<br>週5回以上：¥25,500</td>
              </tr>
              <tr>
                <td><span>ジュニアA</span></td>
                <td>中級</td>
                <td>中学生</td>
                <td class="cell_for_sp">90分</td>
                <td class="cell_for_sp">週1回：¥9,500<br>週2回：¥13,500<br>週3回：¥16,500<br>週4回：¥19,500<br>週5回以上：¥25,500</td>
              </tr>
              <tr>
                <td><span>アッパー</span></td>
                <td>上級</td>
                <td>高校生以上</td>
                <td class="cell_for_sp">90分</td>
                <td class="cell_for_sp">週1回：¥9,500<br>週2回：¥13,500<br>週3回：¥16,500<br>週4回：¥19,500<br>週5回以上：¥25,500</td>
              </tr>
            </tbody>
          </table>
          <p>※入会時に入会金¥13,000が別途必要となります。<br>設備費 月額¥1,000、メザミの会：毎年４月に¥4,000が別途必要になります。<br>対象年齢は目安となります。体験レッスンで経験やレベルを判断し、実際の入学クラスを決定します。</p>
        </div>
        <div>
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/classes/classes_1.jpg">
        </div>
      </section>
    </section>
    <section>
      <h2 class="content-title"><span>コンテンポラリークラス</span></h2>
      <p>近年、バレエの世界ではクラシックとコンテンポラリーの重要度は50/50となっており、国際バレエコンクールなどでも度々その重要性が審査員により熱く語られています。<br>メザミバレエスタジオではクラシックだけでなく、経験豊かな講師がコンテンポラリーの指導も行っています。<br>発表会で踊ったり、希望者はコンクール等のコンテンポラリー部門に積極的に参加させて、子供達の才能をバランス良く伸ばし将来の可能性を広げます。<br>
      </p>
      <section>
        <table>
          <thead>
            <tr>
              <th>クラス名</th>
              <th>レベル</th>
              <th>対象者</th>
              <th>1クラスの時間</th>
              <th>チケット受講</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span>コンテンポラリークラス</span></td>
              <td>初中級以上</td>
              <td>ジュニアB以上のクラスの受講者</td>
              <td>60分</td>
              <td>12枚：¥10,000<br>※1クラス2枚消化</td>
            </tr>
          </tbody>
        </table>
      </section>
    </section>
    <section>
      <h2 class="content-title"><span>選抜クラス</span></h2>
      <p>ジュニアB以上のクラスを週4回受講している生徒からの選抜者のみ受講できるプロ養成クラスです。<br>
        バレエテクニックを中心としたレッスンですが、国内/海外でプロを目指す子供達の為に、講師と生徒が一体となり、肉体面から精神面までトータルサポートを行う特別なクラスです。<br>
        バレエテクニック/表現力/コンテンポラリー/キャラクター/マナー/コミュニケーション能力/人脈サポート/留学サポート/国際コンクール/英語サポート/身だしなみ/ヘアメイク など、プロになる為に必要な術を教えます。</p>
      <section>
        <table>
          <thead>
            <tr>
              <th>クラス名</th>
              <th>レベル</th>
              <th>対象者</th>
              <th>1クラスの時間</th>
              <th>受講料</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span>メザミクラス</span></td>
              <td>プロ養成</td>
              <td>ジュニアB以上のクラスを<br>週4回受講している<br>生徒からの選抜者</td>
              <td>90分</td>
              <td>月4回：¥8,000</td>
            </tr>
          </tbody>
        </table>
      </section>
    </section>
    <section>
      <h2 class="content-title"><span>大人バレエ</span></h2>
      <p>
      レベルに合わせて入門クラスからポアントクラスまで設けています。楽しみながら健康維持したい人や、シェイプアップしたい人、姿勢が気になる人はもちろん、現役プロダンサーから本格的にバレエを習いたい人も納得のクラス。<br>
      大人バレエクラスはチケット制オープンクラスになっており、購入から６ヶ月の有効期限内であれば自由に参加して頂けますので、忙しい方もご予定に合わせて通って頂けます。
      </p>
      <section>
        <table>
          <thead>
            <tr>
              <th>クラス名</th>
              <th>レベル</th>
              <th>対象者</th>
              <th>1クラスの時間</th>
              <th>チケット価格</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><span>大人バレエクラス（通常）</span></td>
              <td>入門以上</td>
              <td rowspan="2">高校生以上</td>
              <td>90分</td>
              <td>12枚：¥23,000</td>
            </tr>
            <tr>
              <td><span>大人バレエクラス（ポアント）</span></td>
              <td>中級以上</td>
              <td class="cell_for_sp">高校生以上</td>
              <td>30分</td>
              <td>12枚：¥10,000</td>
            </tr>
          </tbody>
        </table>
        <p>※上記チケット価格は会員価格となります。入会時に入会金¥13,000が別途必要になります。<br>非会員の方はクラス1回につき、通常クラス¥3,000、ポアントクラス¥1,000となります。<br>前日までの予約が必要です。</p>
      </section>
    </section>
    <section>
      <h2 class="content-title"><span>体験レッスン／個人レッスン／特別クラス／コンクールレッスン</span></h2>
      <p>お1人様 1回、体験レッスンが ¥1,000 で受講できます。入会を検討されている方はご利用ください。また個人レッスンのご相談にも応じます。<br>国内、国外から講師を招いて特別クラスも不定期で開催しています。<br>コンクール参加者はコンクールレッスンがあります。</p>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>