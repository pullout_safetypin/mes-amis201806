<?php /* Template Name: contact */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/contact.css?20241128" type="text/css">
  <script>
    $(document).ready(function(){
      $('.submit_btn').on('click', function(){
        console.log($('.wpcf7-form')[0]);
        $('.wpcf7-form')[0].submit();
      });
    });
  </script>
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section>
      <h1 class="content-title"><span>体験申込/お問い合わせ</span></h1>
      <?php while(have_posts()): the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>