<?php /* Template Name: home */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/404.css?20241128" type="text/css">
</head>
<body>
  <header>
    <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="slider"></section>
  <section id="contents">
    <h1 class="content-title">Page Not Found</h1>
    <p>お探しのページが見つかりませんでした。</p>

  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>