<?php /* Template Name: schedule */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/schedule.css?20241128" type="text/css">
  <script>
    <!--
      $(document).ready(function(){
        if($('span[id^=more-]')){
          $('span[id^=more-]').html('<a href="javascript:void(0);" class="read_more">スケジュールを表示</a>');
          $('span[id^=more-] a').on('click',function(){
            $('#contents img').fadeIn(500);
            $(this).css('opacity','0');
          });          
          $('#contents img').eq(1).css('display','none');
          $('#contents img').eq(2).css('display','none');
        }
      });

    //-->
  </script>
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section>
      <h1 class="content-title"><span>クラススケジュール</span></h1>
      <?php while(have_posts()): the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>