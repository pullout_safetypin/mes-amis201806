//View port control
var ViewPort = {
  width: 'device-width',
  initialScale: '1.0',
  minimumScale: '0.25',
  maximumScale: '1.6',
  modes : {
    sp: {
      width:'device-width',
      initialScale: '1.0'
    },
    pc: {
      width: '1280',
      initialScale: '1.0'

    }
  },
  currentMode : 'sp',
  //ページ読込み時に実行 Cookieから設定を読み出す
  init: function () {
    if ($.cookie('viewport-mode')) {
      var modeName = $.cookie("viewport-mode");
      this.changeMode(modeName);
    }
  },
  //スマホ向けとPC向けを切り替え
  changeMode: function(modeName){
    var mode = this.modes[modeName];
    //$.cookie('viewport-mode', modeName); //Cookieに値を設定
    this.width = mode.width;
    this.initialScale = mode.initialScale;
    this.minimumScale = mode.minimumScale;
    this.maximumScale = mode.maximumScale;
    this.currentMode = modeName;
    this.applyMetaTag();
  },
  //現在の設定でmetaタグを差し替える
  applyMetaTag: function (){
    var content = 'width=' + this.width + ', initial-scale=' + this.initialScale;
    $('meta[name=viewport]').attr('content', content);
  },
};

ViewPort.init();

$(document).ready(function(){
  //fade-in on page scroll.
  $(window).scroll(function(){
    $('.fadein').each(function(){
      var elemPos = $(this).offset().top;
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 150){
          $(this).addClass('scrolled');
      }
    });
  });
  //smooth scroll
  $('a[href^="#"]').on('click',function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });
  //drawer menu
  $('.sp_menu_btn').on('click',function(){
      show_sp_menu();
  });
  $('.sp_close span').on('click',function(){
      hide_sp_menu();
  });
  //View port control button
  $('.switch_viewport_pc').on('click', '', function(){
    hide_sp_menu();
    ViewPort.changeMode('pc');
    console.log('viewport_pc');
  });

  $('.switch_viewport_sp').on('click', '', function(){
    //ViewPort.changeMode('sp');
    console.log('viewport_sp');
  });
  $(window).on('resize',function(){
    if($(window).width() > 768){
      hide_sp_menu();
    }
  });
  set_negative_margin_for_jp_span();
});

//slide
function bridge_vegas(args){
  $("#slider").vegas({
    delay: 5000, //スライドまでの時間(ms)
    timer: false, //タイマーバーの表示/非表示
    transition: 'fade', //スライド間のエフェクトを設定
    transitionDuration: 200, //エフェクト時間をミリ秒単位で設定
    preload:true,
    slides: args //画像URL
  });
}

function show_sp_menu(){
  $('.sp_menu_body').fadeIn('slow');
  var body_height = parseInt($('.sp_menu_body .link').outerHeight(true)) + parseInt($('.sp_menu_body').css('padding-top')) + parseInt($('.sp_menu_body').css('padding-bottom'));
  var window_height = $(window).height() < $(window).width() ? $(window).width(): $(window).height();
  if(window_height < body_height){
	$('body').css('height', body_height);
  }
  else{
	$('body').css('height', window_height);  	
  }
  $('body').addClass('fixed');
}
function hide_sp_menu(){
  $('.sp_menu_body').fadeOut('slow');
  $('body').css('height', '100%');  	
  $('body').removeClass('fixed');
}
function set_negative_margin_for_jp_span(){
  $('header a span.jp').each(function(){
    var margin_value = ($(this).get(0).offsetWidth/-2) + 'px';
    $(this).css('margin-left',margin_value);
  });
}
