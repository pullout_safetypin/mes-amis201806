<?php /* Template Name: access-from-chiyoda-ku */ ?>
<?php get_header(); ?>
  <!-- local style and javascript -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access.css?20241128" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/access-sub.css?20241128" type="text/css">
</head>
<body>
  <header>
  <?php get_template_part('header_menu'); ?>
  </header>
  <section class="sp_menu_body"><?php get_template_part('sp_menu'); ?></section>
  <section id="contents">
    <section id="chiyoda">
    <div class="nav-wrap">
      <h1 class="title only-sp"><span>千代田区からのアクセス</span></h1>
      <picture class="image">
        <source srcset="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-chiyoda-sp.png" media="(max-width:768px)">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/access/map-chiyoda.png" alt="">
      </picture>
      <div>
        <div class="sec-links">
          <h1 class="title only-pc"><span>千代田区からのアクセス</span></h1>
          <div class="by-train">電車でお越しの方</div>
          <ul class="link-list">
            <li class="item"><a href="#chiyoda_kanda01">神田駅から</a></li>
            <li class="item"><a href="#chiyoda_kudanshita01">九段下駅から</a></li>
            <li class="item"><a href="#chiyoda_akihabara01">秋葉原駅から</a></li>
            <li class="item"><a href="#chiyoda_tokyo01">東京駅から</a></li>
            <li class="item"><a href="#chiyoda_iidabashi01">飯田橋駅から</a></li>
            <li class="item"><a href="#chiyoda_zinboucho01">神保町駅から</a></li>
            <li class="item"><a href="#chiyoda_hibiya01">日比谷駅から</a></li>
            <li class="item"><a href="#chiyoda_ochanomizu01">御茶ノ水駅から</a></li>
            <li class="item"><a href="#chiyoda_yuuraukchou01">有楽町駅から</a></li>
            <li class="item"><a href="#chiyoda_mitsuke01">赤坂見附駅から</a></li>
          </ul>
          <div class="by-bus">バスでお越しの方</div>
          <ul class="link-list">
            <li class="item"><a href="#chiyoda_tokyo02">東京駅から</a></li>
          </ul>
          <div class="by-car">車でお越しの方</div>
          <ul class="link-list">
            <li class="item"><a href="#chiyoda_car">東京駅から</a></li>
            <li class="item"><a href="#chiyoda_car">有楽町駅から</a></li>
            <li class="item"><a href="#chiyoda_car">飯田橋駅から</a></li>
            <li class="item"><a href="#chiyoda_car">御茶ノ水駅から</a></li>
          </ul>
        </div>

      </div>
    </div>


      <h2 class="title by-train">電車でお越しの方</h2>
      <section id="chiyoda_kanda01"  class="data-wrap">
        <h3 class="sub-title">神田駅からお越しの方<br class="only-sp">（乗り入れ線： JR京浜東北線、JR中央線、JR山手線、東京メトロ銀座線）</h3>
        <p class="summary">東京メトロ銀座線「神田駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>8分</p>
          <p><span>所要時間</span>15分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="ginza">銀座線</span>「神田駅」乗車</p>
          <p>↓渋谷行き（2駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
        </section>
        <section id="chiyoda_kudanshita01"  class="data-wrap">
        <h3 class="sub-title">九段下駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ東西線、東京メトロ半蔵門線、都営新宿線）</h3>
        <p class="summary">東京メトロ東西線「九段下駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>10分</p>
          <p><span>所要時間</span>17分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="tozai">東西線</span>「九段下駅」乗車</p>
          <p>↓東葉勝田台行き（5駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_akihabara01"  class="data-wrap">
        <h3 class="sub-title">秋葉原駅からお越しの方<br class="only-sp">（乗り入れ線： JR京浜東北線、JR総武線、JR山手線、つくばエクスプレス、東京メトロ日比谷線）</h3>
        <p class="summary">東京メトロ日比谷線「秋葉原駅」より「茅場町駅」下車、東京メトロ東西線「茅場町駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分 </p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>10分</p>
          <p><span>所要時間</span>17分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="hibiya">日比谷線</span>「秋葉原駅」乗車</p>
          <p>↓中目黒行き（3駅）</p>
          <p>東京メトロ<span class="hibiya">日比谷線</span>「茅場町駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「茅場町駅」乗車</p>
          <p>↓東葉勝田台行き（1駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_tokyo01"  class="data-wrap">
        <h3 class="sub-title">東京駅からお越しの方<br class="only-sp">（乗り入れ線：JR上越新幹線、JR東海道新幹線、JR東北新幹線、JR北陸新幹線、JR上野東京ライン、JR京浜東北線、JR京葉線、JR総武線、JR中央線、JR東海道本線、JR山手線、JR横須賀線、東京メトロ丸ノ内線）</h3>
        <p class="summary">東京メトロ丸ノ内線「東京駅」より「大手町駅」下車、東京メトロ東西線「大手町駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>10分</p>
          <p><span>所要時間</span>17分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="marunouchi">丸ノ内線</span>「東京駅」乗車</p>
          <p>↓池袋行き（1駅）</p>
          <p>東京メトロ<span class="marunouchi">丸ノ内線</span>「大手町駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「大手町駅」乗車</p>
          <p>↓東葉勝田台行き（3駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_iidabashi01"  class="data-wrap">
        <h3 class="sub-title">飯田橋駅からお越しの方<br class="only-sp">（乗り入れ線：JR総武線、東京メトロ東西線、東京メトロ南北線、東京メトロ有楽町線、都営大江戸線）</h3>
        <p class="summary">東京メトロ東西線「飯田橋駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>11分</p>
          <p><span>所要時間</span>18分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="tozai">東西線</span>「飯田橋駅」乗車</p>
          <p>↓東葉勝田台行き（6駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_zinboucho01"  class="data-wrap">
        <h3 class="sub-title">神保町駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ半蔵門線、都営新宿線、都営三田線）</h3>
        <p class="summary">東京メトロ半蔵門線「神保町駅」より「清澄白河駅」下車、メザミバレエスタジオまで徒歩10分</p>
        <div class="data">
          <p><span>乗換回数</span>0回</p>
          <p><span>乗車時間</span>10分</p>
          <p><span>所要時間</span>20分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「神保町駅」乗車</p>
          <p>↓押上行き（4駅）</p>
          <p>東京メトロ<span class="hanzomon">半蔵門線</span>「清澄白河駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_hibiya01"  class="data-wrap">
        <h3 class="sub-title">日比谷駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ千代田線、東京メトロ日比谷線、都営三田線）</h3>
        <p class="summary">東京メトロ千代田線「日比谷駅」より「大手町駅」下車、東京メトロ東西線「大手町駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>13分</p>
          <p><span>所要時間</span>20分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="chiyoda-line">千代田線</span>「日比谷駅」乗車</p>
          <p>↓北綾瀬行き（2駅）</p>
          <p>東京メトロ<span class="chiyoda-line">千代田線</span>「大手町駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「大手町駅」乗車</p>
          <p>↓東葉勝田台行き（3駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町」下車</p>
        </div>
      </section>
      <section id="chiyoda_ochanomizu01"  class="data-wrap">
        <h3 class="sub-title">御茶ノ水駅からお越しの方<br class="only-sp">（乗り入れ線：JR総武線、JR中央線、東京メトロ丸ノ内線）</h3>
        <p class="summary">東京メトロ丸ノ内線「御茶ノ水駅」より「大手町駅」下車、東京メトロ東西線「大手町駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>13分</p>
          <p><span>所要時間</span>20分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="marunouchi">丸ノ内線</span>「御茶ノ水駅」乗車</p>
          <p>↓荻窪行き（2駅）</p>
          <p>東京メトロ<span class="marunouchi">丸ノ内線</span>「大手町駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「大手町駅」乗車</p>
          <p>↓東葉勝田台行き（3駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_yuuraukchou01"  class="data-wrap">
        <h3 class="sub-title">有楽町駅からお越しの方<br class="only-sp">（乗り入れ線：JR京浜東北線、JR山手線、東京メトロ有楽町線）</h3>
        <p class="summary">東京メトロ有楽町線「有楽町駅」より「月島駅」下車、都営大江戸線「月島駅」より「門前仲町駅」下車、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>14分</p>
          <p><span>所要時間</span>21分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="yuurakuchou">有楽町線</span>「有楽町駅」乗車</p>
          <p>↓新木場行き（3駅）</p>
          <p>東京メトロ<span class="yuurakuchou">有楽町線</span>「月島駅」下車</p>
          <p><span class="ooedo">都営大江戸線</span>「月島駅」乗車</p>
          <p>↓両国・春日方面（1駅）</p>
          <p><span class="ooedo">都営大江戸線</span>「門前仲町駅」下車</p>
        </div>
      </section>
      <section id="chiyoda_mitsuke01"  class="data-wrap">
        <h3 class="sub-title">赤坂見附駅からお越しの方<br class="only-sp">（乗り入れ線：東京メトロ銀座線、東京メトロ丸ノ内線）</h3>
        <p class="summary">東京メトロ銀座線「赤坂見附駅」より「日本橋駅」下車、東京メトロ東西線「日本橋駅」より「門前仲町駅」、メザミバレエスタジオまで徒歩7分</p>
        <div class="data">
          <p><span>乗換回数</span>1回</p>
          <p><span>乗車時間</span>17分</p>
          <p><span>所要時間</span>24分</p>
        </div> <div class="route">
          <p>東京メトロ<span class="ginza">銀座線</span>「赤坂見附駅」乗車</p>
          <p>↓浅草行き（6駅）</p>
          <p>東京メトロ<span class="ginza">銀座線</span>「日本橋駅」下車</p>
          <p>東京メトロ<span class="tozai">東西線</span>「日本橋駅」乗車</p>
          <p>↓東葉勝田台行き（2駅）</p>
          <p>東京メトロ<span class="tozai">東西線</span>「門前仲町駅」下車</p>
        </div>
      </section>

      <h2 class="title by-bus">バスでお越しの方</h2>
      <section id="chiyoda_tokyo02"  class="data-wrap">
        <h3 class="sub-title">東京駅からお越しの方</h3>
        <p class="summary">「東京駅」丸の内北口出口より徒歩4分、都営バス東22 錦糸町駅行き「東京駅丸の内北口」乗車、「門前仲町」下車、メザミバレエスタジオ徒歩6分</p>
        <div class="data">
          <p><span>乗車時間</span>21分</p>
          <p><span>所要時間</span>31分</p>
        </div> <div class="route">
          <p>「東京駅」　丸の内北口出口</p>
          <p>↓徒歩4分</p>
          <p><span class="toei-bus">都営バス</span> 東22「東京駅丸の内北口」乗車</p>
          <p>↓錦糸町駅前行き （9駅）</p>
          <p><span class="toei-bus">都営バス</span> 東22「門前仲町」下車</p>
        </div>
      </section>

      <h2 class="title by-car">車でお越しの方</h2>
      <section id="chiyoda_car"  class="data-wrap">
        <h3 class="sub-title">東京都千代田区からの所要時間</h3>
        <p class="data">
          東京駅から約12分<br>
          有楽町駅から約15分<br>
          飯田橋駅から約19分<br>
          御茶ノ水駅から約19分
        </p>
      </section>

      <h2 class="title">その他の区からのアクセス</h2>
      <section class="data-wrap">
        <div class="sec-route__links">
          <a class="item" href="/access/from-edogawa-ku/">江戸川区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-sumida-ku/">墨田区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-chuo-ku/">中央区からの<br class="only-sp">アクセス</a>
          <a class="item" href="/access/from-minato-ku/">港区からの<br class="only-sp">アクセス</a>
        </div>
      </section>

    </section>
  </section>
  <footer>
    <?php get_footer(); ?>
  </footer>
</body>
</html>